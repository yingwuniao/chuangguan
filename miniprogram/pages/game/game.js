var t = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
      chuigetanchuguanka:false,
      chuigetanchuhongbao:false,
      chuigeindex:0,
      level:0,
      chuigeerrIndex:[],
      resultStatus: !1,
      sharetype: "guess",
    },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.chuigewentilist();
  },
 
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },
  chuigeLoginCancel: function() {
    this.setData({
      chuigeloginStatus: !1
    });
},
chuigeshowLogin: function() {
  this.setData({
    chuigeloginStatus: !0
  });
},
  chuigeLoginConfirm: function(e) {

},
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },
  chuigewentilist: function () {
    var e = this;
    const db = wx.cloud.database()
    db.collection('question').aggregate().sample({
      size: 10
    }).end().then(res => {
      var data = res.list;
      "collection.aggregate:ok" == res.errMsg && (
        res.list.length > 0 ? e.setData({
        resultStatus: !1,
        chuigewentilist: data,
        chuigeindex: 0,
        chuigewenti: data[0]
      }) : e.setData({
        resultStatus: !1,
        noMoreTopic: !0
      }));
    });
  },
  chuigecheck: function (options) {
    var indexs=options.target.dataset.id;
    if(indexs==this.data.chuigewenti.daan){
      this.setData({
        chuigetanchuguanka: 1,
       })
    }

     
  },
  chuigedown: function() {
    if(this.data.chuigeindex>=this.data.chuigewentilist.length-1){
      this.setData({
        chuigewenti: this.data.chuigewentilist[0],
        chuigetanchuguanka: !1,
        chuigeerrIndex:[],
        level:this.data.level+1,
        chuigeindex:0
      })
    }else{
      this.setData({
        chuigewenti: this.data.chuigewentilist[this.data.chuigeindex+1],
        chuigetanchuguanka: !1,
        chuigeindex:this.data.chuigeindex+1,
        level:this.data.level+1,
      })
    }
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  onShareTimeline: function () {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})