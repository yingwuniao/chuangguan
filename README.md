# 在线闯关答题小程序

#### 介绍

全民闯关答题V2.0小程序是使用云开发的在线答题小程序，无需搭建服务器，无需域名即可使用云端能力。目前该主要用于在线答题游戏，在线闯关答题，个人流量主答题，企业答题活动等场景适合个人或企业上线使用，目前小程序集成微信banner广告，视频广告，插屏广告，激励视频广告等功能，方便运营者赚取广告收益.
  
   注：本开源版本为全民闯关答题V1.0，久供开发学习，如果上线需要经过作者许可哦 ~ 开发不易、创作不易。保障作者著作权益 ~ 如果你觉得项目对你有所帮助 ~ 期待得到你的打赏哦
   
     上线V2.0版本系统可联系作者获取

#### 联系我们
     如需帮助请联系我微信 ，微信：chuige365  备注码云
 [锤哥笔记](http://www.xw115.com)
[闯关答题小程序](http://www.xw115.com/index.php/index/index/appcon/rid/2)
 ##  适用场景

1 ·各行业答题游戏。

2 ·教育培训机构答题拉新。

3 ·个人流量主运营盈利。

4 ·各学科答题竞赛活动。语文、数学、英语、化学、医学等竞赛答题活动

5 ·党建答题活动

6 ·培训题库刷题
  
    答题界面或规则不同请联系作者定制
 
#### V2.0截图
![](https://gitee.com/yingwuniao/img/raw/master/chuangguan/001.png)
![](https://gitee.com/yingwuniao/img/raw/master/chuangguan/002.png)
![](https://gitee.com/yingwuniao/img/raw/master/chuangguan/003.png)
![](https://gitee.com/yingwuniao/img/raw/master/chuangguan/004.png)
![](https://gitee.com/yingwuniao/img/raw/master/chuangguan/005.png)
![](https://gitee.com/yingwuniao/img/raw/master/chuangguan/006.png)
![](https://gitee.com/yingwuniao/img/raw/master/chuangguan/007.png)
![](https://gitee.com/yingwuniao/img/raw/master/chuangguan/008.png)


 
#### V2.0版已完成功能
+ 答题闯关挑战
+ 答题发放红包（可设置几题发放红包，每题设置红包数额）
+ 答题段位设置（自定义段位名称和关卡）
+ 观看激励视频获取金币（限制每天观看次数）
+ 排行榜
+ 红包兑换物品设置
+ 更多软件推广
+ 集成banner 视频广告，插屏广告，激励视频广告等

##### UI界面或答题具体功能可联系作者定制

#### 在线体验

![](https://gitee.com/yingwuniao/img/raw/master/chuangguan/qrcode.png)

##  付费服务
   
    2020-10-10更新，如果您在闯关答题小程序学习遇到问题，我们也提供付费服务，只需要200元便可以成为我们的付费客户，付费客户享受下面优质服务：
+ 全程无忧发布
+ 免费数据导入、导出
+ 少量需求免费定制开发
+ 后续版本免费更新
+ 小程序视频学习课程
+ 其他优质服务

#### 联系我们

   如需帮助请联系我微信 ，微信：chuige365  
   
   ![](https://gitee.com/yingwuniao/img/raw/master/wx.jpg)

#### 打赏作者
   ![](https://gitee.com/yingwuniao/img/raw/master/dashang.jpg)
   
#### 备注
      开源版本为v1.0
https://developers.weixin.qq.com/community/develop/article/doc/0004046f568298d05d1b4fcc65b013
	  